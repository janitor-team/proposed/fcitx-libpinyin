fcitx-libpinyin (0.5.4-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Bump Standards-Version to 4.6.1.
  * debian/control: Recommend installation of fcitx-tools.
    (Closes: #930210)
  * debian/copyright: Remove mentioning of GitLab.

 -- Boyuan Yang <byang@debian.org>  Thu, 25 Aug 2022 15:15:23 -0400

fcitx-libpinyin (0.5.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/upstream/signing-key.asc: Add upstream key.

 -- Boyuan Yang <byang@debian.org>  Fri, 29 Jan 2021 13:24:30 -0500

fcitx-libpinyin (0.5.3-4) unstable; urgency=medium

  * Team upload.
  * Rebuild before Debian 11 release.
  * Bump Standards-Version to 4.5.1.
  * Bump debhelper compat to v13.
  * debian/watch: Monitor download.fcitx-im.org release.

 -- Boyuan Yang <byang@debian.org>  Tue, 26 Jan 2021 11:33:31 -0500

fcitx-libpinyin (0.5.3-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces

  [ Boyuan Yang ]
  * debian: Apply "wrap-and-sort -abst".
  * debian: Bump debhelper compat to v11.
  * debian/control:
    + Revert maintainer name to Debian Input Method Team.
    + Update Vcs-* fields and use git repo under input-method-team.
    + Mark fcitx-libpinyin as Multi-Arch: same.
  * debian/rules: Modernize rules file and use dh_missing --fail-missing.
  * debian/copyright: Update source url with gitlab.com repository.
  * debian/watch: Update url to monitor gitlab.com upstream.

 -- Boyuan Yang <byang@debian.org>  Fri, 19 Oct 2018 21:03:06 -0400

fcitx-libpinyin (0.5.3-2) unstable; urgency=medium

  * Disable QT on platform without qtwebengine support.

 -- YunQiang Su <syq@debian.org>  Wed, 09 May 2018 21:22:41 +0800

fcitx-libpinyin (0.5.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.3.
  * Update Maintainer mail to debian-input-method@lists.debian.org.
  * Add ChangZhuo Chen (陳昌倬) as Uploaders.
  * Update Vcs-* fields to salsa.debian.org.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 01 Feb 2018 10:12:30 +0800

fcitx-libpinyin (0.5.2-1~exp1) experimental; urgency=medium

  * Team upload
  * Import Upstream version 0.5.2
  * Update outdated url
  * d/control:
    + Update Build-Depends
      Drop libqtwebkit-dev, add qtwebengine5-dev (Closes: #784462)
      Bump to libpinyin13-dev
    + Update Standards-Version to 4.1.0

 -- Shengjing Zhu <i@zhsj.me>  Mon, 11 Sep 2017 22:22:25 +0800

fcitx-libpinyin (0.3.3-1) experimental; urgency=medium

  * Imported Upstream version 0.3.3
  * Update B-D, update std-ver

 -- Aron Xu <aron@debian.org>  Fri, 30 Oct 2015 17:54:46 +0800

fcitx-libpinyin (0.3.1-1) unstable; urgency=low

  * New upstream release.

 -- Aron Xu <aron@debian.org>  Sun, 06 Jul 2014 03:35:08 +0800

fcitx-libpinyin (0.2.92-1) unstable; urgency=low

  * Imported Upstream version 0.2.92
  * Remove DMUA stantz
  * Bump build with libpinyin4
  * Add fcitx-bin to build-dep

 -- YunQiang Su <wzssyqa@gmail.com>  Mon, 30 Sep 2013 20:20:26 +0800

fcitx-libpinyin (0.2.1-2) unstable; urgency=low

  * Upload to unstable.
  * Bump standard version to 3.9.4.

 -- YunQiang Su <wzssyqa@gmail.com>  Sat, 16 Feb 2013 17:47:19 +0800

fcitx-libpinyin (0.2.1-1) experimental; urgency=low

  * New upstream release.

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 28 Sep 2012 15:52:07 +0800

fcitx-libpinyin (0.2.0-1) experimental; urgency=low

  [ YunQiang Su ]
  * debian/watch: watch _dict.tar.gz
  * New upstream version 0.2.0

  [ Aron Xu ]
  * Use dh compat 9, update build dependencies.

 -- Aron Xu <aron@debian.org>  Sat, 04 Aug 2012 23:13:35 +0800

fcitx-libpinyin (0.1.1-2) unstable; urgency=low

  * compatible with 0.6.91

 -- YunQiang Su <wzssyqa@gmail.com>  Sat, 26 May 2012 16:48:38 +0800

fcitx-libpinyin (0.1.1-1) unstable; urgency=low

  * Initial release (Closes: #658145)

 -- Aron Xu <aron@debian.org>  Thu, 03 May 2012 18:23:02 +0000
